FROM golang:alpine AS build

LABEL com.botornot.version="0.0.3"
LABEL vendor="leonjohnson"
LABEL com.botornot.release-date="2018-03-23"
LABEL com.botornot.version.is-production=""

WORKDIR /go/src/app
COPY . .

# Get dependancies
RUN apk add --no-cache git
RUN go get -d -v ./...
RUN go install -v ./...

# Set ENV for scratch
ENV GOOS=linux
ENV GOARCH=386
# Build the executable
RUN go build -o /bin/app

# Create a single layer image
FROM scratch
COPY --from=build /bin/app /bin/app
CMD ["/bin/app"]

# Set default port
EXPOSE 8080
