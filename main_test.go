package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestMain(t *testing.T) {
	//
}

func TestTrueLoveGenerator(t *testing.T) {
	actual := trueLoveGenerator()
	if len(actual) < 3 {
		t.Errorf("got wrong length string: got %v", len(actual))
	}
}

func BenchmarkTrueLoveGenerator(b *testing.B) {
	for n := 0; n < b.N; n++ {
		a := trueLoveGenerator()
		_ = a
	}
}

func TestLookingForLoveInAllTheWrongPlaces(t *testing.T) {
	// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
	// pass 'nil' as the third parameter.
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}
	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(lookingForLoveInAllTheWrongPlaces)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}
