package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"

	"github.com/rs/xid"
)

func trueLoveGenerator() string {
	rand.Seed(time.Now().UTC().UnixNano())
	love := []string{
		"bot",
		"person",
	}
	return love[rand.Intn(len(love))]
}

func health(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func lookingForLoveInAllTheWrongPlaces(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, `{"id":"%s","love":"%s"}`, xid.New(), trueLoveGenerator())
}

func main() {
	http.HandleFunc("/", lookingForLoveInAllTheWrongPlaces)
	http.HandleFunc("/health", health)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
